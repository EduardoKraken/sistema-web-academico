import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import store from '@/store'

import Login                        from '@/views/usuario/Login.vue'
import Perfil                       from '@/views/usuario/Perfil.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [

    { path: '/'               , name: 'Home'           , component: Home, 
      meta: { ADMIN: true, USUARIO: true, NORMAL: true, libre: true }},

  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)) {
    next()
  } else {
    next({
      name: 'Home'
    })
  }
})

export default router