import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

import VueResource from "vue-resource";


Vue.use(VueResource);

// Vue.http.options.root = 'http://localhost:3004/';

new Vue({

  // created() {
  //   Vue.http.interceptors.push((request, next) => {
  //     request.headers.set('Authorization', 'Bearer ');
  //     request.headers.set('Accept', 'application/json');
  //     request.headers.set('Access-Control-Allow-Origin', '*');
  //     request.headers.set('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE');
  //     request.headers.set('Access-Control-Allow-Headers', 'Content-Type,Authorization');
  //     next();
  //   });

  //   Vue.http.options.root = 'http://localhost:3004/';
  // },

  router,
  store,
  vuetify,
  render: function(h) {
    return h(App);
  },
}).$mount("#app");