import Vue from 'vue';
import Vuex, { MutationTree, ActionTree, GetterTree } from 'vuex';
import router from '@/router';
import vuetify from '@/plugins/vuetify';

Vue.use(Vuex);

interface State {
  escuela: any | null;
}

const state: State = {
  escuela: null,
};

const mutations: MutationTree<State> = {
  ESCUELA(state, value) {
    state.escuela = value;
  },
};

const actions: ActionTree<State, any> = {
  guardarInfo({ commit }, escuela) {
    commit('ESCUELA', escuela);
  },
};

const getters: GetterTree<State, any> = {
  getEscuela(state) {
    return state.escuela;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
