'use strict'

import { app, protocol, BrowserWindow, shell, ipcMain } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer'
const isDevelopment = process.env.NODE_ENV !== 'production'

const fs       = require('fs');
const path     = require('path');
const express  = require('express')
const { exec } = require('child_process');
const os       = require('os');

// Crear el servidor Express
const expressApp = express();

// Define la ruta a los archivos estáticos
const rutaImagenesCampanias = 'D:/proyectos/galeria/';

// Sirve archivos estáticos desde la ruta
expressApp.use('imagenes-campanias', express.static(rutaImagenesCampanias));

const server = expressApp.listen(3000, () => {
  console.log('Servidor Express iniciado en el puerto 3000');
});

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])

async function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      // Use pluginOptions.nodeIntegration, leave this alone
      // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
      nodeIntegration: (process.env
          .ELECTRON_NODE_INTEGRATION as unknown) as boolean,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION
    }
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    win.loadURL(`app://./index.html`);
  }

  // Manejar eventos de pantalla completa
  win.on('enter-full-screen', () => {
    console.log('pantalla completa')
    // Agregar lógica para ajustar el contenido cuando se entra en pantalla completa
  });

  win.on('leave-full-screen', () => {
    console.log('pantalla min')
    // Agregar lógica para restaurar el diseño cuando se sale de pantalla completa
  });
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    server.close();
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installExtension(VUEJS_DEVTOOLS)
    } catch (e) {
      console.error('Vue Devtools failed to install:', e )
    }
  }
  createWindow()
})

ipcMain.on('open-local-app', (event, path) => {
  shell.openPath(path).then(() => {
    console.log('Aplicación local abierta');
  }).catch((error) => {
    console.error('Error al abrir la aplicación local:', error);
  });
});

ipcMain.on('get-local-image', (event, imagenName ) => {
  const imagePath = `${rutaImagenesCampanias}${ imagenName }`;
  const imageBase64 = fs.readFileSync(imagePath, 'base64');
  event.reply('local-image-data', imageBase64);
});

// Función para las imagenes individuales
ipcMain.on('get-local-image-unique', (event, imagenName ) => {
  const imagePath = `${rutaImagenesCampanias}${ imagenName }`;
  const imageBase64 = fs.readFileSync(imagePath, 'base64');
  event.reply('local-image-data-unique', imageBase64);
});

ipcMain.on('info-sistema', (event, path) => {

  console.log(1)

  exec('wmic logicaldisk get size,freespace,caption', (error: Error | null, stdout: string) => {
    if (error) {
      console.error(`Error al obtener espacio en disco: ${error.message}`);
      return;
    }

    // Aquí puedes enviar la información a la ventana del renderer
    console.log( stdout )
  });

  const infoSistema = {
    plataforma: os.platform(),
    version: os.version(),
    arquitectura: os.arch(),
    memoriaTotal: os.totalmem(),
    memoriaLibre: os.freemem(),
  };

  console.log( infoSistema )
  event.reply('info-sistema-respuesta', infoSistema);
});


// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

// Función para obtener información del espacio en disco
exec('wmic logicaldisk get size,freespace,caption', (error: Error | null, stdout: string) => {
if (error) {
  console.error(`Error al obtener espacio en disco: ${error.message}`);
  return;
}

// Aquí puedes enviar la información a la ventana del renderer
console.log( stdout )
});

