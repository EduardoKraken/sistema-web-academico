const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: [
    'vuetify'
  ],

  configureWebpack: {
    resolve: {
      // ...
      fallback: {
        "fs": require.resolve("path-browserify"),
        "path": require.resolve("path-browserify"),
        // 👇️👇️👇️ add this 👇️👇️👇️
        // "os": true,
        // "path": true,
      }
    }
  },

  pluginOptions: {
    electronBuilder: {
      nodeIntegration: true
    }
  }

})
